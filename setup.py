# This file relies on https://github.com/pypa/sampleproject
# as a draft implementation that is 'pretty good' to start with.

from setuptools import setup, find_packages

PROJECT_DESCRIPTION = "pyDanilov is a Python3 implementation of the Danilov model, \
a semi-empirical model of the non-auroral Earth's ionosphere D-Region."

PROJECT_DESCRIPTION_LONG = """
pyDanilov is a Python3 implementation of the Danilov model,
a semi-empirical model of the non-auroral Earth's ionosphere D-Region.
Reference: Danilov, Rodevich, and Smirnova, Adv. Space Res.  
15, #2, 165, 1995,   https://doi.org/10.1016/S0273-1177(99)80042-8

Input:
- z    - solar zenith angle in degrees   
- it   - season (month)   
- f    - F10.7 solar radio flux (daily)
- vKp  - Kp magnetic index (3-hour)
- f5SW - indicator for Stratospheric Warming (SW) conditions
=0 no SW, =0.5 minor SW, =1 major SW
- f6WA - indicator for Winter Anomaly (WA) conditions
 =0 no WA, =0.5 weak WA, =1 strong WA

Criteria for SW and WA indicators:
- SW minor:  Temperature increase at the 30 hPa level by 10 deg.
- SA major:  The same but by 20 degrees.
- WA weak:   An increase of the absorption in the 2-2.8 MHz range at short A3 paths by 15 dB
- WA strong: The same by 30 dB.

Only for month 12 to 2 (winter).

Output:
- numpy array of electron density [m-3] at h=60,65,70,75,80,85, and 90km
"""

setup(
    # this will be the package name you will see, e.g. the output of 'conda list' in anaconda prompt
    name='pydanilov',
    # Versions should comply with PEP 440, https://www.python.org/dev/peps/pep-0440/
    version='0.0.7b8',
    description=PROJECT_DESCRIPTION,
    long_description=PROJECT_DESCRIPTION_LONG,
    long_description_content_type='text/markdown', #- some bug with markdown - displayed not correctly
    url='https://gitlab.com/zolotov/pydanilov',  # homepage
    license='Apache Software License',  # to remove 'license: UNKNOWN' notice
    #platform=['any'],
    author='Denis Sakaev, Oleg Zolotov',
    author_email='denissackaev@yandex.ru',

    # For a list of valid classifiers, see https://pypi.org/classifiers/
    classifiers=[
        # How mature is this project? Common values are
        'Development Status :: 2 - Pre-Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Physics',

        # License
        'License :: OSI Approved :: Apache Software License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate you support Python 3. These classifiers are *not*
        # checked by 'pip install'.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3 :: Only',
    ],

    keywords='lower ionosphere of the Earth, D-region, D-layer, model',

    # This field lists other packages that your project depends on to run.
    # Any package you put here will be installed by pip when your project is
    # installed, so they must be valid existing projects.
    #
    # For an analysis of "install_requires" vs pip's requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=['numpy'],  # Optional

    # Automatically finds out all directories (packages) - those must contain a file named __init__.py (can be empty)
    packages=find_packages()  # include/exclude arguments take * as wildcard, . for any sub-package names

    #,package_data={'pyfiri': [r'_ext/_data/firi2018ed3.nc']},
    #include_package_data=True
)

# To generate wheel, a command for the terminal:
# python setup.py bdist_wheel
# twine upload --repository testpypi dist/*
